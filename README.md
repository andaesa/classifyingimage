This repository includes 2 examples of how to cassify image 

You will find 2 folders/directories named "classifywithKeras" and "classifywithcustomclassifier"

1) classifywithKeras

I did this classification by referencing to this website : http://www.pyimagesearch.com/2016/08/10/imagenet-classification-with-python-and-keras/

You will find in the folder these files : "test_imagenet.py" , "imagenet_utils.py" , "vgg16.py" and a directory named "images"

"test_imagenet.py" will be the main code, "vgg16.py" is the Keras model and "imagenet_utils.py" is the code to initialize vgg16. Besides VGG16, you also can
clone this repository : https://github.com/fchollet/deep-learning-models to get Keras model for VGG19.
The directory "images" contains 3 sample image to be use with the main code.

To run the code, place "test_imagenet.py","imagenet_utils.py","vgg16.py"(or "vgg19.py" if you want to use that instead "vgg16.py") and your directory of sample image(taken from camera or from the internet)
in one new directory.

Open up terminal(if you on Ubuntu Ctrl + Alt + T), go to the new directory containing all the files mentioned above, and type

    python test_imagenet.py --image images/<image file name>.<file type - jpg or png>
    
    
2) classifywithcustomclassifier

For this type of classification, I followed tutorial given from this website : https://pythonprogramming.net/haar-cascade-object-detection-python-opencv-tutorial/

You will find in the folder/directory these files : "findimage.py" , "object_detect.py" , "watch5050.jpg" , "watchcascade10stage.xml"

"findimage.py" is the code that we use to find the all the negative image that will be use on the training of our custom classifier. The code will find all the negative images from given url
and stored them in the directory "neg". This code also contains function to delete all unclear negative image that we got from the url. There is also another function in this code that will create a descriptor
file for the negative images named bg.txt

###Note : At the time of writing this, the image-net.org page is currently down. The example url that I provided in the "findimage.py" is from image-net.org. If you can't get enough negative images,
the person who make the tutorial provided the reader with negative images already stored in a directory on the tutorial website.

"watch5050.jpg" is used to create positive samples. First, you need to place the directory of negative images, "bg.txt" and "watch5050.jpg" inside a new directory. Go to that directory and create 
another new directory named "info" to store "info.lst"(the positive samples). Then run this command on your terminal

    opencv_createsamples -img watch5050.jpg -bg bg.txt -info info/info.lst -pngoutput info -maxxangle 0.5 -maxyangle 0.5 -maxzangle 0.5 -num 1950
    
What this does is creates samples, based on the image we specifiy, bg is the background information, info where we will put the info.list output (which is a lot like the bg.txt file), 
then the -pngoutput is wherever we want to place the newly generated images. Finally, we have some optional parameters to make our original image
a bit more dynamic and then =num for the number of samples we want to try to create.

Next we create the vector files from our positive images. Basically this means that we will stich all of our positive images together. To do that run the following command on the terminal

    opencv_createsamples -info info/info.lst -num 1950 -w 20 -h 20 -vec positives.vec
    
We let it know where is the info file, how many images we want to put in the file, and what dimensions should the image be in the vector file. You can make the dimension larger but it might take
a longer time to train the classifier.

Before we start the training, we create another new directory named "data" to store the .xml file. After that, check your currently working directory whether it includes 
the following files and directories or not: "info"(directory), "data"(directory), the directory of negative images, "positives.vec", "bg.txt", "watch5050.jpg"

Now is the training step. Go to your currently working directory on terminal and run this command

    opencv_traincascade -data data -vec positives.vec -bg bg.txt -numPos 1800 -numNeg 900 -numStages 10 -w 20 -h 20
    
Here we tell it where the output data will be saved, where is the vector file and the backgroud file. Note that we use significantly 
less numPos than we have. This is to make room for the stages, which will add to this.

For the number of positives and negatives, generally people will follow the common rule to use 2:1 ratio of positives to negatives. For the stage, we set it to 10. You can set it to 10-15 but 
the higher the stage, the more time it will take to train.

After the training is finished, you will find a file named "cascade.xml" inside the "data" directory. I renamed this to "watchcascade10stage.xml" and move it to other new directory for testing.
Inside that new directory, I put together "object_detect.py" with the "watchcascade10stage.xml"

Open up terminal, go to that directory and execute 

    python object_detect.py

It will open a window from your webcam. Show your watch, it will draw a rectangle on the window indicating that the script recogize your watch. In case it did not happen, you can use
your own image to replace "watch5050.jpg" and create samples, create vector files and train. The image does not have to be watch. It can be any image.

###Note : you need to have have at least 2GB of RAM(Random Access Memory) in your system in order to train