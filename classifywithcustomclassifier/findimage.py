# import necessary packages
import urllib
import cv2
import numpy as np
import os

# link is from the dataset of image-net.org
# the function will save all the negative image from given url and save it
# inside directory 'neg'
def store_raw_images():
	req ='image-net.org/api/text/imagenet.synset.geturls?wnid=n07942152'
	response = urllib.urlopen(req).read().decode()
	
	# rename the negative image starting from number 1
	pic_num = 1

	# make the directory name 'neg' if the directory not exists
	if not os.path.exists('neg'):
		os.makedirs('neg')
	
	# for each negative image received, resize it
	for i in response.split('\n'):
		try:
			print(i)
			urllib.urlretrieve(i, "neg/"+str(pic_num)+".jpg")
			img = cv2.imread("neg/"+str(pic_num)+".jpg",cv2.IMREAD_GRAYSCALE)
			resized_image = cv2.resize(img, (100,100))
			cv2.imwrite("neg/"+str(pic_num)+".jpg",resized_image)
			pic_num += 1
		
		except Exception as e:
			print(str(e))
			
# this function is to get rid any unclear image
# make new directory 'ugly', drag one example of unclear image
# before running this function
def find_uglies():
	match = False
	for file_type in ['neg']:
		for img in os.listdir(file_type):
			for ugly in os.listdir('ugly'):
				try:
					current_image_path = str(file_type)+'/'+str(img)
					ugly = cv2.imread('ugly/'+str(ugly))
					question = cv2.imread(current_image_path)
					if ugly.shape == question.shape and not(np.bitwise_xor(ugly, question).any()):
						print('That is one ugly pic! Deleting!')
						print(current_image_path)
						os.remove(current_image_path)
				except Exception as e:
					print(str(e))

# this function will create bg.txt
def create_pos_n_neg():
	for file_type in ['neg']:
		
		for img in os.listdir(file_type):
			if file_type == 'neg':
				line = file_type+'/'+img+'\n'
				with open('bg.txt', 'a') as f:
					f.write(line)
			elif file_type == 'pos':
				line = file_type+'/'+img+'1 0 0 50 50\n'
				with open('info.dat','a')as f:
					f.write(line)

# run all the function above seperately by commenting out
# by commenting out these one of these 3 lines
					
#store_raw_images()
#find_uglies()
#create_pos_n_neg()
